<?php

namespace App\Http\Controllers;

use App\Models\Memebresia;
use App\Models\Paquetes;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function register()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $user = User::create([
            'uuid' => Str::uuid(),
            'name' => $request->name,
            'lastName' => $request->lastName,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $paquete_free = Paquetes::where('proof', 1)->first();

        if($paquete_free != null)
        {
            $date = Carbon::parse(date('Y-m-d'));
            $endDate = $date->addDay($paquete_free->daysDuration);
            $membresia = Memebresia::create([
                'uuid' => Str::uuid(),
                'idPackage' => $paquete_free->id,
                'idUser' => $user->id,
                'start' => date('Y-m-d'),
                'end' => $endDate,
                'idPay' => 0
            ]);

            $user->idMembresia = $membresia->id;
            $user->save();
        }

        Auth::loginUsingId($user->id);

        return redirect()->route('home');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('raiz');
    }
}
