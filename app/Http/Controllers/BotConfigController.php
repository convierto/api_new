<?php

namespace App\Http\Controllers;

use App\Models\BotConfig;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class BotConfigController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $config = BotConfig::where('uuidUser', Auth::user()->uuid)->first();

        if($config == null)
        {
            $config = BotConfig::create([
                'uuid' => Str::uuid(),
                'uuidUser' => Auth::user()->uuid,
                'name' => 'Nombre de tu Chatbot',
                'welcome_message' => 'Bienvenido soy tu asesor, cómo puedo apoyarte?',
                'goodbye_message' => 'Gracias por tu preferencia.',
                'status' => 1
            ]);
        }

        return view('bots.config', compact('config'));
    }

    public function update(Request $request, $botConfig)
    {
        $config = BotConfig::whereUuid($botConfig)->first();
        $config->name = $request->name;
        $config->welcome_message = $request->welcome_message;
        $config->goodbye_message = $request->goodbye_message;
        $config->status = $request->status;
        $config->save();

        return back()->with('success', 'Actualizado correcto, en pocos minutos podras visualizarlo en tu boot');
    }

}
