<?php

namespace App\Http\Controllers;

use App\Models\{Memebresia, User};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user()->typeUser == 2)
        {
            $titulo1 = "Usuarios Activos";
            $titulo2 = "Membresias Activas";
            $data1 = User::count();
            $data2 = Memebresia::where('end', '>', date('Y-m-d'))->count();

            return view('home', compact(['data1', 'data2', 'titulo1', 'titulo2']));
        }

        if(Auth::user()->typeUser == 1)
        {
            $titulo1 = "Total de clics";
            $titulo2 = "Total de contactos";
            $data1 = User::count();
            $data2 = Memebresia::where('end', '>', date('Y-m-d'))->count();

            return view('home', compact(['data1', 'data2', 'titulo1', 'titulo2']));
        }
    }
}
