<?php

namespace App\Http\Controllers;

use App\Models\Memebresia;
use Illuminate\Http\Request;
use App\Models\Paquetes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PaquetesController extends Controller
{
    public function index()
    {
        $paquetes = Paquetes::all();

        return response()->json([
            'data' => $paquetes,
            'message' => 'Paquetes Obtenidos con Exito',
            'status' => 200
        ]);
    }


    public function store(Request $request)
    {
        $stripe = new \Stripe\StripeClient(
            'sk_test_51MRivTEigSsp7sX62Cj4CJZ2sAeWjBCvCYLDIjp8rI05HZBnPHwwrHKXvGnegD3ah3CsW4S81O5pr8nZw9tXXkhq00FTH700ll'
        );
        $paquetes = Paquetes::where('proof', 1)->count();

        if ($paquetes > 0 && $request->proof == 1) {
            return response()->json([
                'data' => '',
                'message' => 'Ya existe un paquete FREE o de activacion automatica, intente nuevamente!',
                'status' => 201
            ]);
        }

        $product_stripe = $stripe->products->create([
            'name' => $request->title,
            'description' => $request->description,
        ]);

        $price_stripe = $stripe->prices->create([
            'unit_amount' => $request->price * 100,
            'currency' => 'usd',
            'product' => $product_stripe->id
        ]);

        $paquete = Paquetes::create([
            'uuid' => Str::uuid(),
            'price' => $request->price,
            'title' => $request->title,
            'description' => $request->description,
            'daysDuration' => $request->daysDuration,
            'proof' => $request->proof,
            'product_code_stripe' => $product_stripe->id,
            'price_code_stripe' => $price_stripe->id,
            'status' => 1
        ]);

        return response()->json([
                'data' => $paquete,
                'message' => 'Paquete Registrado Con Exito!',
                'status' => 200
            ]);
    }

    public function edit($paquete)
    {
        $paquete = Paquetes::whereUuid($paquete)->first();

        return view('packages.edit', compact('paquete'));
    }

    public function update(Request $request, $paquete)
    {
        $paquete = Paquetes::whereUuid($paquete)->first();

        if (!isset($request->proof)) {
            $paquete->proof = 0;
        }

        $paquete->price = $request->price;
        $paquete->title = $request->title;
        $paquete->description = $request->description;
        $paquete->daysDuration = $request->daysDuration;
        $paquete->save();

        return redirect()
            ->route('packages')
            ->with('success', 'Paquete editado con exito!');
    }

    public function destroy($paquete)
    {
        $paquete = Paquetes::whereUuid($paquete)->delete();

        return 200;
    }

    public function contratar()
    {
        $paquetes = Paquetes::where('proof', null)
            ->orWhere('proof', 0)
            ->get();
        return view('members.list', compact('paquetes'));
    }

    public function upgrade()
    {
        $actual = Memebresia::find(Auth::user()->idMembresia);
        $paquetes = Paquetes::where('proof', null)
            ->orWhere('proof', 0)
            ->where('id', '!=', $actual->idPackage)
            ->get();
        return view('members.list', compact('paquetes'));
    }

    public function desactivar($paquete)
    {
        $paquete = Paquetes::whereUuid($paquete)->first();
        $paquete->status = 2;
        $paquete->save();

        return response()->json([
                'data' => $paquete,
                'message' => 'Paquete Desactivado Correctamente!',
                'status' => 200
            ]);
    }

    public function activar($paquete)
    {
        $paquete = Paquetes::whereUuid($paquete)->first();
        $paquete->status = 1;
        $paquete->save();

        return response()->json([
                'data' => $paquete,
                'message' => 'Paquete Activado Correctamente!',
                'status' => 200
            ]);
    }
}
