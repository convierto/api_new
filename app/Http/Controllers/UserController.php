<?php

namespace App\Http\Controllers;

use App\Models\Memebresia;
use App\Models\Paquetes;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function index() :JsonResponse
    {
        $users = User::with(['membresia', 'membresia.detallePaquete'])->get();

        return response()->json([
            'data' => $users,
            'message' => 'Usuarios Obtenidos con exito!',
            'status' => 200
        ]);
    }


    public function store(Request $request)
    {
       $user = User::create([
            'uuid' => Str::uuid(),
            'name' => $request->name,
            'lastName' => $request->lastName,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'web' => $request->web,
            'phone' => $request->phone,
            'birthday' => $request->date,
            'typeUser' => $request->typeUser
        ]);

        $paquete_free = Paquetes::where('proof', 1)->first();

        if($paquete_free != null)
        {
            $date = Carbon::parse(date('Y-m-d'));
            $endDate = $date->addDay($paquete_free->daysDuration);
            $membresia = Memebresia::create([
                'uuid' => Str::uuid(),
                'idPackage' => $paquete_free->id,
                'idUser' => $user->id,
                'start' => date('Y-m-d'),
                'end' => $endDate,
                'idPay' => 0
            ]);

            $user->idMembresia = $membresia->id;
            $user->save();
        }

        return response()->json([
            'data' => $user,
            'message' => 'Usuario Registrado con exito',
            'status' => 200
        ]);
    }

    public function edit($user)
    {
        $user = User::whereUuid($user)->first();
        return response()->json([
            'data' => $user,
            'message' => 'Usuario Obtenido con exito',
            'status' => 200
        ]);
    }

    public function update(Request $request, $user)
    {
        $user = User::whereUuid($user)->first();
        $user->name = $request->name;
        $user->lastName = $request->lastName;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->web = $request->web;
        $user->birthday = $request->date;
        $user->save();

        return response()->json([
            'data' => $user,
            'message' => 'Usuario Actualizado con exito',
            'status' => 200
        ]);
    }

    public function updateClave(Request $request, $user)
    {
        $user = User::whereUuid($user)->first();
        $user->password = Hash::make($request->password);
        $user->save();
        $nombre = $user->name . ' ' . $user->lastName;
        return response()->json([
            'data' => $user,
            'message' => 'Contraseña de '. $nombre . ' actualizada con exito',
            'status' => 200
        ]);
    }

    public function editProfile()
    {
        $user = Auth::user();
        return view('users.edit-profile', compact('user'));
    }
}
