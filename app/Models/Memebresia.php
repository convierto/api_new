<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Memebresia extends Model
{
    use HasFactory;

    protected $fillable = [
        'idPackage',
        'idUser',
        'start',
        'end',
        'idPay',
        'uuid'
    ];

    public function detallePaquete()
    {
        return $this->hasOne(Paquetes::class, 'id', 'idPackage');
    }

    public function detallePago()
    {
        return $this->hasOne(Pay::class, 'id', 'idPay');
    }
}
