<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paquetes extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'price',
        'title',
        'description',
        'daysDuration',
        'proof',
        'product_code_stripe',
        'price_code_stripe',
        'status'
    ];
}
