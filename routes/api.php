<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['cors']], function () {
    Route::get('users', [UserController::class, 'index']);
});


Route::get('delete-package/{paquete}', [PaquetesController::class, 'destroy']);
Route::post('payment/{package}/{usuario}', [PayController::class, 'paymentProcess']);

Route::post('createSesion', [PayController::class, 'create']);



Route::group([ 'prefix' => 'auth' ], function () {
    Route::post('login', [AuthApiController::class, 'login']);
    Route::post('signup', [AuthApiController::class, 'signUp']);

    Route::group([ 'middleware' => 'auth:api' ], function() {
        Route::post('logout', [AuthApiController::class, 'logout']);
        Route::post('user', [AuthApiController::class, 'user']);
    });
});

// Route::controller(UserController::class)->prefix('users')->group( function () {
//     // Route::get('/', 'index');
//     Route::post('/', 'store');
//     Route::get('data/{user}', 'edit');
//     Route::post('updatePassword/{user}', 'updateClave');
//     Route::post('update/{user}', 'update');
// });

Route::controller(MemebresiaController::class)->prefix('memberships')->group( function () {
    Route::get('/', 'index');
    Route::get('user/{user}', 'show');
});

Route::controller(PayController::class)->prefix('pays')->group( function () {
    Route::get('/', 'index');
    Route::get('user/{user}', 'show');
});

Route::controller(PaquetesController::class)->prefix('packages')->group( function () {
    Route::get('/', 'index');
    Route::post('/', 'store');
    Route::get('desactivar/{paquete}', 'desactivar');
    Route::get('activar/{paquete}', 'activar');
});

Route::controller(TemplatesController::class)->prefix('templates')->group( function () {
    Route::get('/', 'index');
    Route::post('/', 'store');
    Route::get('details/{template}', 'show');
    Route::post('update/{template}', 'update');
    Route::get('desactivar/{template}', 'desactivar');
    Route::get('activar/{template}', 'activar');
});

Route::controller(TemplatesMessagesController::class)->prefix('messages')->group( function () {
    Route::post('/', 'store');
    Route::get('/get/{template}', 'index');
    Route::get('/delete/{messages}', 'delete');
    Route::get('/details/{messages}', 'show');
    Route::post('/update/{message}', 'update');
});


